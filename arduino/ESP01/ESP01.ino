#include <ESP8266WiFi.h>

#include <FirebaseArduino.h>

#include <NTPClient.h>
#include <WiFiUdp.h>

#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>

/* ----- ----- */

// IFS =>   SSID   : Bbox-A658B231
//     => Password : 2AC13AEA5E44D6D7A2ADC116E5325A

// Solier =>   SSID   : SFR_80F8
//        => Password : atemallotte2reostrat

#define WIFI_SSID "SFR_80F8"
#define WIFI_PASSWORD "atemallotte2reostrat"

#define FIREBASE_HOST "miniserre-14ca.firebaseio.com"
#define FIREBASE_AUTH ""

#define NTP_OFFSET   60 * 60      // In seconds
#define NTP_INTERVAL 60 * 1000    // In miliseconds
#define NTP_ADDRESS  "europe.pool.ntp.org"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

void setup() {
  Serial.begin(9600);
  
  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("\nconnecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  timeClient.begin();
  
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void loop() {
  if (Serial.available()) {    
    String readSerial = Serial.readString();

    int codeSerre = getValue(readSerial, ';', 0).toInt();

    float temperatureIn = getValue(readSerial, ';', 1).toFloat();
    float humuditeIn = getValue(readSerial, ';', 2).toFloat();
    float humiditeSoil = getValue(readSerial, ';', 3).toFloat();
    float temperatureEx = getValue(readSerial, ';', 4).toFloat();
    float humiditeEx = getValue(readSerial, ';', 5).toFloat();

    int electricite = getValue(readSerial, ';', 6).toInt();
    int eau = getValue(readSerial, ';', 7).toInt();

    Serial.print("Code serre : ");
    Serial.print(codeSerre);
    Serial.print(" => TempIn = ");
    Serial.print(temperatureIn);
    Serial.print(", HumIn = ");
    Serial.print(humuditeIn);
    Serial.print(", HumSoil = ");
    Serial.print(humiditeSoil);
    Serial.print(", TempEx = ");
    Serial.print(temperatureEx);
    Serial.print(", HumEx = ");
    Serial.print(humiditeEx);
    Serial.print(", Elec = ");
    Serial.print(electricite);
    Serial.print(", Eau = ");
    Serial.println(eau);
    
    if (codeSerre && temperatureIn && humuditeIn && humiditeSoil && temperatureEx && humiditeEx) {
      Serial.println("Send data !\n");
      sendDataWeb(codeSerre, temperatureIn, humuditeIn, humiditeSoil, temperatureEx, humiditeEx, electricite, eau);
    }
  }
}

void sendDataWeb(int codeSerre, float temperatureIn, float humuditeIn, float humiditeSoil, float temperatureEx, float humiditeEx, int electricite, int eau) {
  timeClient.update();
  unsigned long timeStamp = timeClient.getEpochTime();

  String path = "codeSerre/" + String(codeSerre) + "/dataRecord/" + String(timeStamp) + "/";

  // Set value codeSerre
  Firebase.set("codeSerre/" + String(codeSerre) + "/codeSerre", codeSerre);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error codeSerre");
    ESP.deepSleep(0);
    return;
  }
  
  // Set value inData temperature
  Firebase.setFloat(path + "/inData/temperature", temperatureIn);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error TempIn");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);
  // Set value inData humidity
  Firebase.setFloat(path + "/inData/humidity", humuditeIn);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error HumIn");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);
  // Set value inData humiditySoil
  Firebase.setFloat(path + "/inData/humiditySoil", humiditeSoil);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error HumSoil");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);

  // Set value outData temperature
  Firebase.setFloat(path + "/outData/temperature", temperatureEx);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error TempEx");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);
  // Set value outData humidity
  Firebase.setFloat(path + "/outData/humidity", humiditeEx);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error HumEx");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);

  // Set value electricite
  Firebase.setInt("codeSerre/" + String(codeSerre) + "/setting/electricityLevel", electricite);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error Elec");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);

  // Set value eau
  Firebase.set("codeSerre/" + String(codeSerre) + "/setting/waterLevel", eau);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error Eau");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);

  // Set value timiStamp
  Firebase.set(path + "/timiStamp", timeStamp);
  // handle error
  if (Firebase.failed()) {
    Serial.print("Error TimeStamp");
    ESP.deepSleep(0);
    return;
  }
  delay(1000);

  if (electricite == 0 || eau == 0) {

    // // get value 
    String mail = Firebase.getString("codeSerre/" + String(codeSerre) + "/email");
    delay(1000);

    if(mail) {    
      HTTPClient http;    //Declare object of class HTTPClient
  
      //GET Data
      String Link = "http://enzohondareite.alwaysdata.net/mail.php?electricity=" + String(electricite) + "&water=" + String(eau) + "&email=" + mail;
  
      http.begin(Link);     //Specify request destination
  
      int httpCode = http.GET();            //Send the request
      String payload = http.getString();    //Get the response payload
  
      Serial.println("TEST");
      Serial.println(httpCode);   //Print HTTP return code
      Serial.println(payload);    //Print request response payload
  
      http.end();  //Close connection
    }
  } 

  delay(4000);

  ESP.deepSleep(0);
}

String getValue(String data, char separator, int index){
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}
