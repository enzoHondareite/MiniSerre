// ----- Librairie ----- //
#include "dht.h" // Librairie pour les DHT22
#include <Servo.h> // Librairie pour le servoMoteur de la porte
#include <Wire.h>              // Librairie pour l'écran I2C
#include <LiquidCrystal_I2C.h> // Librairie pour l'écran I2C
#include <SoftwareSerial.h> // Librairie pour la communication avec l'ESP01


// ----- Pin composants ----- //

/* ----- DHT22 ----- */
#define pinCapteurDHT_Ex 2
#define pinCapteurDHT_In 3
/* ----- DHT22 ----- */

/* ----- Niveau eau ----- */
#define pinCapteurEau_haut 4  
#define pinCapteurEau_bas 5
/* ----- Niveau eau ----- */

/* ----- Fil résistif ----- */
#define pinFilResistif 1
/* ----- Fil résistif ----- */

/* ----- Pompe ----- */
#define pinPompe 6
/* ----- Pompe ----- */

/* ----- Ventilateur ----- */
#define pinVentilateur 7
/* ----- Ventilateur ----- */

/* ----- Servo porte ----- */
#define pinServoPorte 9
/* ----- Servo porte ----- */

/* ----- ESP01 ----- */
#define pinReveilESP01 12
/* ----- ESP01 ----- */

/* ----- Bouton + Ok - ----- */
#define pinBoutonMoins 8
#define pinBoutonOk A3
#define pinBoutonPlus A2
/* ----- Bouton + Ok - ----- */

/* ----- Commande solaire ----- */
#define pinRelayBatterie 10
#define pinRelaySolaire 11
/* ----- Commande solaire ----- */

/* ----- Humidite sol ----- */
#define pinCapteurHumiditeSol A0
/* ----- Humidite sol ----- */

/* ----- Tension batterie ----- */
#define pinTensionBatterie A1
/* ----- Tension batterie ----- */



// ----- Setup composants ----- //

/* ----- Ecran I2C ----- */
LiquidCrystal_I2C lcd(0x27, 16, 2);
/* ----- Ecran I2C ----- */

/* ----- DHT22 ----- */
DHT DHT_Ex;
DHT DHT_In;
/* ----- DHT22 ----- */

/* ----- Servo porte ----- */
Servo servoPorte;
/* ----- Servo porte ----- */

/* ----- ESP01 ----- */
SoftwareSerial SerialESP01(12, 13); // RX, TX
/* ----- ESP01 ----- */



// ----- Variables ----- //

/* ----- Identifiant Serre ----- */
const int codeSerre = 5323;
/* ----- Identifiant Serre ----- */

/* ----- Menu ----- */
boolean stateBoutonMoins,
        stateBoutonOK,
        stateBoutonPlus;

boolean lastStateBoutonMoins,
        lastStateBoutonOk,
        lastStateBoutonPlus;

boolean stateVeille = 0;

unsigned long clickMillis = 0; // Veille
int timeOut = 10000;

byte temperature = 18; // mini => 18 ; max => 30
byte humidite = 80; // mini => 30 ; max => 80
byte newTemperature,
	   newHumidite;

int stateMenu = 0;

String confirmType = "";
/* ----- Menu ----- */
