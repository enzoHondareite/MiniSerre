// ----- Fichier pour le ventilateur ----- //

/* ----- Utilisation ----- 
 *
 * Ventilateur on  => commandeVentilateur("on");
 * Ventilateur off => commandeVentilateur("off");
 * 
 */

void commandeVentilateur(String etat) {
  if (etat == "on") {
    digitalWrite(pinVentilateur, HIGH);
    return;
  } else if (etat == "off") {
    digitalWrite(pinVentilateur, LOW);
    return;
  } else {
    return;
  }
}
