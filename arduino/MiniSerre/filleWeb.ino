// ----- Fichier pour l'envoie des données sur l'ESP ----- //

/* ----- Utilisation ----- 
 *
 * Envoie données => envoieESP(codeSerre, tempIn, humIn, humTerre, tempEx, humEx, Tbat, Neau);
 * 
 */

void envoieESP(int codeSerre, float temperatureIn, float humuditeIn, float humiditeSoil, float temperatureEx, float humiditeEx, int electricite, int eau) {
  String data = String(codeSerre) + ";" + String(temperatureIn) + ";" + String(humuditeIn) + ";" + String(humiditeSoil)+ ";" + String(temperatureEx) + ";" + String(humiditeEx) + ";" + String(electricite) + ";" + String(eau);

  SerialESP01.begin(9600);

  digitalWrite(pinReveilESP01, LOW);
  delay(10);
  digitalWrite(pinReveilESP01, HIGH);
  
  delay(10000);
  
  SerialESP01.println(data);
  delay(500);
  SerialESP01.end();
}
