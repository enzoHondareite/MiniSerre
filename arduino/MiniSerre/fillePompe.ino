// ----- Fichier pour la pompe ----- //

/* ----- Utilisation ----- 
 *
 *  Pompe on => commandePompe("on");
 * Pompe off => commandePompe("off");
 * 
 */

void commandePompe(String etat) {
  if (etat == "on") {
    digitalWrite(pinPompe, HIGH);
    return;
  } else if (etat == "off") {
    digitalWrite(pinPompe, LOW);
    return;
  } else {
    return;
  }
}
