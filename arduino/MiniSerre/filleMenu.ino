// ----- Fichier pour le menu ----- //

/* ----- structure ----- 
 *
 * 0: Accueil
 *
 * 1: Temperature
 * 2: Humidite
 * 
 * 3: Set temperature
 * 4: Set humidite
 * 
 * 5: Validation
 *
 * 6: Salade auto
 * 7: Radis auto
 * 8: Tomate auto
 * 
 */

void affichageMenu() {
  switch (stateMenu) {
    case 0: 
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("MiniSerre");
      lcd.setCursor(4, 1);
      lcd.print("Accueil");
    break;
    case 1:
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("MiniSerre");
      lcd.setCursor(2, 1);
      lcd.print("Temperature");
    break;
    case 2:
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("MiniSerre");
      lcd.setCursor(4, 1);
      lcd.print("Humidite");
    break;
    case 3:
      lcd.clear();
      lcd.setCursor(1, 0);
      lcd.print("Temperature :");
      lcd.setCursor(1, 1);
      lcd.print("-");
      lcd.setCursor(7, 1);
      lcd.print(temperature);
      lcd.setCursor(14, 1);
      lcd.print("+");
    break;
    case 4:
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("Humidite :");
      lcd.setCursor(1, 1);
      lcd.print("-");
      lcd.setCursor(7, 1);
      lcd.print(humidite);
      lcd.setCursor(14, 1);
      lcd.print("+");
    break;
    case 5:
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("Validation");
      lcd.setCursor(1, 1);
      lcd.print("-            +");
    break;
    case 6:
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("Mode auto");
      lcd.setCursor(5, 1);
      lcd.print("Salade");
    break;
    case 7:
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("Mode auto");
      lcd.setCursor(5, 1);
      lcd.print("Radis");
    break;
    case 8:
      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print("Mode auto");
      lcd.setCursor(5, 1);
      lcd.print("Tomate");
    break;
  }

  delay(600);
}

void inputMenu(String type, String button) {
  if (type == "temperature") {
    if (button == "less" && newTemperature > 16) {
      newTemperature--;
    } else if (button == "ok" ) {
      confirmType = "temperature";
      return;
    } else if (button == "more" && newTemperature < 42) {
      newTemperature++;
    }
    
    lcd.setCursor(1, 1);
    lcd.print("-");
    lcd.setCursor(7, 1);
    lcd.print(newTemperature);
    lcd.setCursor(14, 1);
    lcd.print("+");
  } else if (type == "humidite") {
    if (button == "less" && newHumidite > 25) {
      newHumidite--;
    } else if (button == "ok" ) {
      confirmType = "humidite";
      return;
    } else if (button == "more" && newHumidite < 90) {
      newHumidite++;
    }

    lcd.setCursor(1, 1);
    lcd.print("-");
    lcd.setCursor(7, 1);
    lcd.print(newHumidite);
    lcd.setCursor(14, 1);
    lcd.print("+");
  } else {
    return;
  }
  delay(180);
}

void actionBouton() {
  switch(stateMenu) {
    case 0: 
      if (lastStateBoutonPlus){stateMenu = 1;}
      affichageMenu();
    break;
    case 1:
      if (lastStateBoutonMoins){stateMenu = 0;}
      if (lastStateBoutonOk){stateMenu = 3; newTemperature=temperature;}
      if (lastStateBoutonPlus){stateMenu = 2;}
      affichageMenu();
    break;
    case 2: 
      if (lastStateBoutonMoins){stateMenu = 1;}
      if (lastStateBoutonOk){stateMenu = 4; newHumidite=humidite;}
      if (lastStateBoutonPlus){stateMenu = 6;}
      affichageMenu();
    break;
    case 6:
      if (lastStateBoutonMoins){stateMenu = 2;}
      if (lastStateBoutonOk){stateMenu = 0; temperature = 18; humidite = 75;}
      if (lastStateBoutonPlus){stateMenu = 7;}
      affichageMenu();
    break;
    case 7:
      if (lastStateBoutonMoins){stateMenu = 6;}
      if (lastStateBoutonOk){stateMenu = 0; temperature = 15; humidite = 75;}
      if (lastStateBoutonPlus){stateMenu = 8;}
      affichageMenu();
    break;
    case 8:
      if (lastStateBoutonMoins){stateMenu = 7;}
      if (lastStateBoutonOk){stateMenu = 0; temperature = 20; humidite = 80;}
      affichageMenu();
    break;
    case 3: 
      if (lastStateBoutonMoins){inputMenu("temperature", "less");}
      if (lastStateBoutonOk){inputMenu("temperature", "ok");stateMenu=5;affichageMenu();}
      if (lastStateBoutonPlus){inputMenu("temperature", "more");}
    break;
    case 4:
      if (lastStateBoutonMoins){inputMenu("humidite", "less");}
      if (lastStateBoutonOk){inputMenu("humidite", "ok");stateMenu=5;affichageMenu();}
      if (lastStateBoutonPlus){inputMenu("humidite", "more");}
    break;
    case 5:
      if (lastStateBoutonMoins){stateMenu = 0;}
      if (lastStateBoutonPlus){
        stateMenu = 0;
        if (confirmType == "temperature") {
          temperature = newTemperature;
        } else if (confirmType == "humidite") {
          humidite = newHumidite;
        }
        confirmType = "";
      }
      affichageMenu();
    break;
  }
}

void initMenu() {
  lcd.backlight();
  lcd.setCursor(5,0);
  lcd.print("Start");
  lcd.setCursor(3,1);
  lcd.print("Arduino !");
  delay(1000);
  lcd.clear();

  affichageMenu(); 
}
