// ----- Fichier pour le fil résistif ----- //

/* ----- Utilisation ----- 
 *
 *  FilRésistif on => commandeFilReistif("on");
 * FilRésistif off => commandeFilReistif("off");
 * 
 */

void commandeFilReistif(String etat) {
  if (etat == "on") {
    digitalWrite(pinFilResistif, HIGH);
    return;
  } else if (etat == "off") {
    digitalWrite(pinFilResistif, LOW);
    return;
  } else {
    return;
  }
}
