// ----- Fichier pour la tension de la batterie ----- //

/* ----- Utilisation ----- 
 *
 * Donnée tension batterie => mesureTensionBatterie(); (float)
 * 
 */


float mapfloat(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float mesureTensionBatterie() {
  int dataTension = analogRead(pinTensionBatterie);
  // Conversion 0v - 5v => 0v - 15v
  float tension  = mapfloat(dataTension, 0.0, 1023.0, 0.0, 15.32);

  return tension;
}
