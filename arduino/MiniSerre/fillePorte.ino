// ----- Fichier pour la commande de la porte ----- //

/* ----- Utilisation ----- 
 *
 * Ouverture => commandePorte("ouvert");
 * Fermeture => commandePorte("ferme");
 * 
 */

void commandePorte(String etat){
  if (etat == "ouvert") {
    for (int position = 0; position <= 180; position++) {
      servoPorte.write(position);
      delay(15);
    }
    return;
  } else if (etat == "ferme") {
    for (int position = 180; position >= 0; position--) {
      servoPorte.write(position);
      delay(15);
    }
    return;
  } else {
    return;
  }
}