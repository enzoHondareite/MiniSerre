#include "variable.h"

// Include automatique des fichier .ino

void setup() { 
  /* ----- DHT22 ----- */
  pinMode(pinCapteurDHT_Ex, INPUT);
  pinMode(pinCapteurDHT_In, INPUT);
  /* ----- DHT22 ----- */

  /* ----- Niveau eau ----- */
  pinMode(pinCapteurEau_haut, INPUT_PULLUP);
  pinMode(pinCapteurEau_bas, INPUT_PULLUP);
  /* ----- Niveau eau ----- */

  /* ----- Fil résistif ----- */
  pinMode(pinFilResistif,  OUTPUT);
  /* ----- Fil résistif ----- */

  /* ----- Pompe ----- */
  pinMode(pinPompe, OUTPUT);
  /* ----- Pompe ----- */

  
  /* ----- Bouton + Ok - ----- */
  pinMode(pinBoutonMoins, INPUT_PULLUP);
  pinMode(pinBoutonOk, INPUT_PULLUP);
  pinMode(pinBoutonPlus, INPUT_PULLUP);
  /* ----- Bouton + Ok - ----- */

  /* ----- Commande solaire ----- */
  pinMode(pinRelayBatterie, OUTPUT);
  pinMode(pinRelaySolaire, OUTPUT);
  /* ----- Commande solaire ----- */

  /* ----- Servo porte ----- */
  servoPorte.attach(pinServoPorte);
  servoPorte.write(0);
  /* ----- Servo porte ----- */

  /* ----- ESP01 ----- */
  pinMode(pinReveilESP01, OUTPUT);
  digitalWrite(pinReveilESP01, HIGH);
  /* ----- ESP01 ----- */

  /* ----- Ventilateur ----- */
  pinMode(pinVentilateur, OUTPUT);
  /* ----- Ventilateur ----- */

  /* ----- Humidite sol ----- */
  pinMode(pinCapteurHumiditeSol, INPUT);
  /* ----- Humidite sol ----- */

  /* ----- Tension batterie ----- */
  pinMode(pinTensionBatterie, INPUT);
  /* ----- Tension batterie ----- */

  /* ----- Ecran I2C ----- */
  lcd.init();
  initMenu();
  /* ----- Ecran I2C ----- */
}

void loop() {
  boolean stateBoutonMoins = !digitalRead(pinBoutonMoins);
  boolean stateBoutonOK = !digitalRead(pinBoutonOk);
  boolean stateBoutonPlus = !digitalRead(pinBoutonPlus);

  if (stateBoutonMoins || stateBoutonOK || stateBoutonPlus) {
    clickMillis = millis(); // Temps veille
    if (stateVeille == 0) {
      lcd.backlight(); // Sortie de veille
      stateVeille = 1;
      delay(500);
    } else if (stateVeille == 1) {     
      lastStateBoutonMoins = stateBoutonMoins;
      lastStateBoutonOk = stateBoutonOK;
      lastStateBoutonPlus = stateBoutonPlus;
      
      actionBouton();
    }
  }

  if (clickMillis + timeOut < millis()) {
    lcd.noBacklight(); // Mise en veille
    stateVeille = 0;
  }

  delay(15);
}
