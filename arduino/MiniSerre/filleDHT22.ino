// ----- Fichier pour le DHT22 ----- //

/* ----- Utilisation ----- 
 *
 *  Mesure intérieur => dataStructDHT dataDHT_Ex = readDHT_Ex();
 * Données intérieur => dataDHT_Ex.temperature / dataDHT_Ex.humidite
 * 
 *  Mesure extérieur => dataStructDHT dataDHT_In = readDHT_In();
 * Données extérieur => dataDHT_In.temperature / dataDHT_In.humidite
 *
 */

struct dataStructDHT {
  float temperature;
  float humidite;
};

struct dataStructDHT readDHT_Ex() {
   dataStructDHT dataDHT_Ex;

  int chk = DHT_Ex.read22(pinCapteurDHT_Ex);
  
  dataDHT_Ex.temperature = DHT_Ex.temperature;
  dataDHT_Ex.humidite = DHT_Ex.humidity;

   if (isnan(dataDHT_Ex.temperature) || isnan(dataDHT_Ex.humidite)) {
    return;
  }
  return dataDHT_Ex;
}

struct dataStructDHT readDHT_In() {
   dataStructDHT dataDHT_In;

  int chk = DHT_In.read22(pinCapteurDHT_In);

  dataDHT_In.temperature = DHT_In.temperature;
  dataDHT_In.humidite = DHT_In.humidity;

   if (isnan(dataDHT_In.temperature) || isnan(dataDHT_In.humidite)) {
    return;
  }
  return dataDHT_In;
}
