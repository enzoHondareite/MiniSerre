// ----- Fichier pour les capteurs de niveau Eau ----- //

/* ----- Utilisation ----- 
 *
 * Donnée niveau eau => mesureNiveauEau();
 * 
 */

int mesureNiveauEau() {
  boolean dataCapteurEau_haut = !digitalRead(pinCapteurEau_haut);
  boolean dataCapteurEau_bas = !digitalRead(pinCapteurEau_bas);

  int niveauEau;
  
  if (dataCapteurEau_haut && dataCapteurEau_bas) {
    int niveauEau = 2; // Niveau haut
  } else if (!dataCapteurEau_haut && dataCapteurEau_bas) {
    int niveauEau = 1; // Niveau moyen
  } else if (!dataCapteurEau_haut && !dataCapteurEau_bas) {
    int niveauEau = 0; // Niveau bas
  } else {
    return;
  }
  return niveauEau;
}
