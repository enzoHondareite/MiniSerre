// ----- Fichier pour le capteur d'humidité su sol ----- //

/* ----- Utilisation ----- 
 *
 * Donnée humidité sol => readHumiditeSol();
 * 
 */

int mesureHumiditeSol() {
  int dataCapteurHumiditeSol = analogRead(pinCapteurHumiditeSol);
  // Conversion de 0% - 100%
  int humiditeSol = map(dataCapteurHumiditeSol, 0, 1023, 0, 100);

  return humiditeSol;
}
