<?php

	$levelElectricity = $_GET["electricity"]; // Argument URL pour le niveau de électrcité
	$levelWater = $_GET["water"]; // Argument URL pour le niveau d'eau
	$email = $_GET["email"]; // Argument URL pour l'email destinataire

	// Sélection du message de l'email suivant les arguments reçus
	if ($levelElectricity == 0 || $levelWater == 0) {
		if ($levelElectricity == 0 && $levelWater != 0) {
			// Problème d'électricité
			$message = 'Elle rencontre un problème, le niveau d\'électricité est faible moins de 25% !';
		}else if ($levelElectricity != 0 && $levelWater == 0) {
			// Problème d'eau
			$message = 'Elle rencontre un problème, le niveau d\'eau est faible moins de 25% !';
		}else if ($levelElectricity == 0 && $levelWater == 0) {
			// Problème d'électricité et d'eau
			$message = 'Elle rencontre des problèmes, le niveau d\'électricité et le niveau d\'eau est faible moins de 25% !';
		}
	}
 
    ini_set( 'display_errors', 1 );
    error_reporting( E_ALL );

    // Email de l'expéditeur
    $from = "enzohondareite@alwaysdata.net";
 
    $subject = "Votre MiniSerre à besion de vous !";

    $headers = "From:" . $from;
 	
 	// Envoie du mail
    mail($email,$subject,$message, $headers);
 
    echo "L'email a été envoyé.";
?>