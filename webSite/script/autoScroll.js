/*
 * Code JavaScript pour gerer le déroulement (scroll) de la page cran par cran
 * 
 * Framwork utilisé : Fullpage.js
 * 
 * Détails : - "$(document).ready" = Attend que le document soit chargé pour exuter le script
 *           - "$('#fullpage').fullpage" = mise en place de Fullpage
 *           - "lockAnchors" = block l'url qui normalement change à chaque section de page
 */

$(document).ready(function () {
    $('#fullpage').fullpage({
        lockAnchors: true,
        navigation: false,
        scrollingSpeed: 500,
        scrollBar: true
    });
});
