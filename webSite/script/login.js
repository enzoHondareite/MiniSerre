// Fonction qui shake en cas d'erreur
jQuery.fn.shake = function (intShakes, intDistance, intDuration) {
    this.each(function () {
        $(this).css("position", "relative");
        for (var x = 1; x <= intShakes; x++) {
            $(this).animate({
                    left: (intDistance * -1)
                }, (((intDuration / intShakes) / 4)))
                .animate({
                    left: intDistance
                }, ((intDuration / intShakes) / 2))
                .animate({
                    left: 0
                }, (((intDuration / intShakes) / 4)));
        }
    });
    return this;
};

// Fonction qui bloque le scroll
function noscroll() {
    window.scrollTo(0, 0);
}

var codeSerreInput = "";

var array_JSON_codeSerre = [];
var JSON_data;

var requestURL = 'https://miniserre-14ca.firebaseio.com/codeSerre/.json';
var request = new XMLHttpRequest(); // Création d'un nouvel objet XMLHttpRequest
request.open('GET', requestURL); // Création d'une nouvelle requête
request.responseType = 'json';
request.send(); // Envoie de la requête
request.onload = function () { // Réponse du serveur
    JSON_data = request.response; // Récupération du JSON

    // Fonction for pour boucler et récupérer les informations
    for (var x in JSON_data) {
        var temp = JSON_data[x]['codeSerre'];
        array_JSON_codeSerre.push(temp);
    }
}


// Fonction lis les cookies
function getCookie(name) {
    var selector = name + '=';
    var cookieArrray = document.cookie.split(';');
    for (var i = 0; i < cookieArrray.length; i++) {
        var cookie = cookieArrray[i];
        while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1, cookie.length);
        }
        if (cookie.indexOf(selector) == 0) {
            return cookie.substring(selector.length, cookie.length);
        }
    }
    return null;
}

// Check si le site est connecté
function checkConnect() {
    var codeSerre = getCookie("codeSerre");
    var serreConnected = getCookie("serreConnected");

    if (codeSerre != "" && serreConnected === "true") {
        readDatabase(codeSerre);
        setTimeout(function () {
            $('div').remove('#connect');
            displayGraphEx(codeSerre);
            displayGraphIn(codeSerre);
        }, 4000);
    } else {
        var newInputCode = $('<div id="connect"><input id="inputCode" type="text" placeholder="code de la miniserre" maxlength="4"></div>');

        document.getElementsByClassName("section")[0].style.filter = "blur(5px)";
        document.getElementsByClassName("section")[1].style.filter = "blur(5px)";
        document.getElementsByClassName("section")[2].style.filter = "blur(5px)";
        window.addEventListener('scroll', noscroll);
        window.addEventListener('touchmove', noscroll);

        $('.bg1').before(newInputCode);

        modifyInput();
    }
}

// Fonction qui crée le cookie de connexion
function setCookie(codeSerre, serreConnected) {
    // Recupération des argument de fonction
    var codeSerre = codeSerre;
    var serreConnected = serreConnected;

    // Variable qui determine le temps de reconnexion
    var timeOut = 1000 * 60 * 60 * 24 * 30;
    // ----------- ms -- s -- m -- h -- j -- = 30 jours

    // Creation du temps de reconnexion
    var date = new Date;
    date.setTime(date.getTime() + timeOut);
    var expires = date.toGMTString();

    // Création des cookie
    document.cookie = 'codeSerre=' + codeSerre + ';expires=' + expires + ';path=/';
    document.cookie = 'serreConnected=' + serreConnected + ';expires=' + expires + ';path=/';
}

function modifyInput() {
    // Bloque les caractéres du code de serre autre que a-f A-F 0-9
    $('#inputCode').on('keypress', function (e) {

        var passSpecialRegex = /[a-fA-F0-9]/;
        var blockSpecialRegex = /[&é"'(-è_çà]/;

        var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);

        if (passSpecialRegex.test(key) === false && blockSpecialRegex.test(key)) {
            e.preventDefault();
            return false;
        }
    });

    // Espace et met en majuscule le code de la serre
    $('#inputCode').on('keyup', function () {

        var inputCode = document.getElementById("inputCode");

        if (inputCode.value.length == 4) {
            testCodeSerre();
        } else if (inputCode.value.length > 0) {
            inputCode.style.letterSpacing = "20px";
            inputCode.style.textTransform = "uppercase";
            inputCode.style.borderColor = '';
        } else {
            inputCode.style.letterSpacing = "";
            inputCode.style.textTransform = "lowercase";
        }

    });
}

function testCodeSerre() {
    var newInputMail = $('<input id="inputMail" type="email" placeholder="Votre adresse mail"/>');
    var newInputMdp = $('<input id="inputMdp" type="text" placeholder="Votre mot de passe"/>')

    codeSerreInput = $('#inputCode').val();
    codeSerreInput = parseInt(codeSerreInput, 16);

    if (array_JSON_codeSerre.includes(codeSerreInput)) {
        if (JSON_data[codeSerreInput]['email'] && JSON_data[codeSerreInput]['password']) {
            $('#inputCode').remove();
            $('#connect').prepend(newInputMdp);
            connect(codeSerreInput);
        } else {
            $('#connect').css('height', '20vmax');
            $('#inputCode').remove();
            $('#connect').prepend(newInputMdp);
            $('#connect').prepend(newInputMail);
            inputAcount(codeSerreInput);
        }
    } else {
        inputCode.style.borderColor = 'red';
        $('#inputCode').shake(3, 7, 800);
    }
}

function connect(codeSerre) {
    $('#inputMdp').keypress(function (e) {
        if (e.which == 13) {
            var password = $('#inputMdp').val();
            password = MD5(password);
            if (password == JSON_data[codeSerre]['password']) {
                $('div').remove('#connect');

                setCookie(codeSerre, true);

                readDatabase(codeSerre);
                setTimeout(function () {
                    displayGraphEx(codeSerre);
                    displayGraphIn(codeSerre);
                    document.getElementsByClassName("section")[0].style.filter = "blur(0)";
                    document.getElementsByClassName("section")[1].style.filter = "blur(0)";
                    document.getElementsByClassName("section")[2].style.filter = "blur(0)";
                    window.removeEventListener('scroll', noscroll);
                    window.removeEventListener('touchmove', noscroll);
                }, 4000);

            } else {
                $('#inputMdp').shake(3, 7, 800);
            }
        }
    });
}

function confirmInput(codeSerre, email, password) {
    var confirmBox = $('<div id="confirm"><button class="btn btn-success" id="trueBox" type="button">Confirmé</button><button class="btn btn-danger" id="falseBox" type="button">Retour</button></div>');
    $('#connect').append(confirmBox);

    $('#trueBox').click(function () {
        $('div').remove('#confirm');
        createAccount(codeSerre, email, password);
        setCookie(codeSerre, true);

        readDatabase(codeSerre);
        $('div').remove('#connect');
        setTimeout(function () {
            displayGraphEx(codeSerre);
            displayGraphIn(codeSerre);
            document.getElementsByClassName("section")[0].style.filter = "blur(0)";
            document.getElementsByClassName("section")[1].style.filter = "blur(0)";
            document.getElementsByClassName("section")[2].style.filter = "blur(0)";
            window.removeEventListener('scroll', noscroll);
            window.removeEventListener('touchmove', noscroll);
        }, 4000);
        return true
    });
    $('#falseBox').click(function () {
        $('div').remove('#confirm');
    });
}

function inputAcount(codeSerre) {
    $('#inputMdp').keypress(function (e) {
        if (e.which == 13) {
            var email = $('#inputMail').val();
            var password = $('#inputMdp').val();
            password = MD5(password);

            confirmInput(codeSerre, email, password);
        }
    });
}
