/*
 * Code JavaScript pour gerer la connection et récupérer les données à la base donnée Firebase (google)
 * 
 * Framwork utilisé : Firebase.js
 * 
 * Détails : -
 *
 */

// Variable de configuration connection pour firebase
var config = {
    apiKey: "AIzaSyAzDYVfLATiCZY1-6bsiL2qKilgTWexPrM",
    authDomain: "miniserre-14ca.firebaseapp.com",
    databaseURL: "https://miniserre-14ca.firebaseio.com",
    projectId: "miniserre-14ca",
    storageBucket: "miniserre-14ca.appspot.com",
    messagingSenderId: "483859650337"
};


// Démarrage de Firebase
firebase.initializeApp(config);

function createAccount(codeSerre, email, password) {
    const dbRef_serre = firebase.database().ref().child('codeSerre/' + codeSerre);
    dbRef_serre.update({
        email: email,
        password: password
    });
}
