// Fonction pour choisir le badge Bootstrap pour le niveau d'eau et électricité
function selectworkingOrder(levelWater, levelElectricity) {
    // Variables pour stocké les états de fonctionnement
    var stateLevelElectricity;
    // 0 = mauvais (0% - 33%), 1 = moyen (33% - 67%), 2 = correct (67% - 100%)

    // Condition pour choisir l'état du niveau d'électricité
    if (levelElectricity >= 67) {
        stateLevelElectricity = 2;
    } else if (levelElectricity >= 33 && levelElectricity < 67) {
        stateLevelElectricity = 1;
    } else if (levelElectricity < 33) {
        stateLevelElectricity = 0;
    }

    // Condition pour appliquer le badge global
    if ((levelWater === 0 && stateLevelElectricity === 0) || (levelWater >= 1 && stateLevelElectricity === 0) || (levelWater === 0 && stateLevelElectricity >= 1)) {
        // Etat mauvais
        $('.workingOrder').text('mauvais');
        $('.workingOrder').addClass('badge-danger');
        $('.workingOrder').removeClass('badge-succes');
        $('.workingOrder').removeClass('badge-warning');
    } else if ((levelWater === 1 && stateLevelElectricity === 1) || (levelWater === 2 && stateLevelElectricity === 1) || (levelWater === 1 && stateLevelElectricity === 2)) {
        // Etat moyen
        $('.workingOrder').text('moyen');
        $('.workingOrder').addClass('badge-warning');
        $('.workingOrder').removeClass('badge-succes');
        $('.workingOrder').removeClass('badge-danger');
    } else if (levelWater === 2 && stateLevelElectricity === 2) {
        // Etat correct
        $('.workingOrder').text('correct');
        $('.workingOrder').addClass('badge-success');
        $('.workingOrder').removeClass('badge-warning');
        $('.workingOrder').removeClass('badge-danger');
    }

    // Condition pour appliquer le badge du niveau d'eau
    switch (levelWater) {
        case 0: // Etat mauvais
            $('#show_waterLevel').addClass('badge-danger');
            $('#show_waterLevel').removeClass('badge-succes');
            $('#show_waterLevel').removeClass('badge-warning');
            break;
        case 1: // Etat moyen
            $('#show_waterLevel').addClass('badge-warning');
            $('#show_waterLevel').removeClass('badge-succes');
            $('#show_waterLevel').removeClass('badge-danger');
            break;
        case 2: // Etat correct
            $('#show_waterLevel').addClass('badge-success');
            $('#show_waterLevel').removeClass('badge-warning');
            $('#show_waterLevel').removeClass('badge-danger');
            break;
    }

    // Condition pour appliquer le badge du niveau d'électricité
    switch (stateLevelElectricity) {
        case 0: // Etat mauvais
            $('#show_electricityLevel').addClass('badge-danger');
            $('#show_electricityLevel').removeClass('badge-succes');
            $('#show_electricityLevel').removeClass('badge-warning');
            break;
        case 1: // Etat moyen
            $('#show_electricityLevel').addClass('badge-warning');
            $('#show_electricityLevel').removeClass('badge-succes');
            $('#show_electricityLevel').removeClass('badge-danger');
            break;
        case 2: // Etat correct
            $('#show_electricityLevel').addClass('badge-success');
            $('#show_electricityLevel').removeClass('badge-warning');
            $('#show_electricityLevel').removeClass('badge-danger');
            break;
    }
}

// Fonction pour calculer l'autonomie
function autonomyBat(levelElectricity) {
    var capacityBattery = 3000; // Capacité en mA.h
    var consomation = 54; // Consomation en mA
    // Calcul de l'autonomie
    var autonomy = Math.round((capacityBattery * (levelElectricity / 100)) / consomation);
    // Insertion des données dans la page HTML
    $('#show_autonomy').text(autonomy);
}
