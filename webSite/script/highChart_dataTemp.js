function displayGraphEx(codeSerre) {
    // Graphique pour les données extérieur : température, humidité
    var chart_dataEx = Highcharts.chart('showDataEx', {
        chart: {
            type: 'spline', // Ligne lisse
            backgroundColor: 'rgba(0, 0, 0, 0.35)',
            animation: {
                duration: 500,
            },
        },

        title: {
            text: 'Température de la serre',
            style: {
                color: '#FFF',
            },
        },

        xAxis: {
            type: 'datetime',
            title: {
                text: 'Heure',
                style: {
                    color: '#FFF',
                },
            },
            labels: {
                style: {
                    color: '#FFF',
                },
            },
            tickInterval: 600000, // Intervalle des labels en timestamp (10 min)
        },

        yAxis: [{
            title: {
                text: 'Température (°C)',
                style: {
                    color: '#FFF',
                    fontSize: '13px',
                },
            },
            labels: {
                style: {
                    color: '#7CB5EC',
                    fontSize: '13px',
                    fontWeight: 'bold',
                },
                format: '{value} °C', // Terminaison du label
            }
        }],

        tooltip: {
            shared: true,
            headerFormat: 'Heure : <b>{point.x:%H:%M}</b><br/>', // Titre tooltip = "Heure : H:M:S"
            pointFormat: '{series.name}: <b>{point.y}</b><br>' // Tooltip = Température : y / Humidité : y
        },

        legend: {
            itemStyle: {
                color: '#FFF',
            },
        },

        plotOptions: { // Ligne
            series: {
                lineWidth: 4,
                marker: {
                    radius: 7, // Point
                }
            }
        },

        series: [
            {
                yAxis: 0,
                name: 'Tempéraure serre',
                data: data_tempIn,
                tooltip: {
                    valueSuffix: ' °C',
                },
                color: '#FF0000'
            },
            {
                yAxis: 0,
                name: 'Température extérieur',
                data: data_tempOut,
                tooltip: {
                    valueSuffix: ' °C'
                }
            }
        ],


        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500,
                },
                chartOptions: {
                    legend: {
                        enabled: false,
                    },
                    plotOptions: {
                        series: {
                            lineWidth: 3,
                            marker: {
                                radius: 4, // Point
                            },
                        },
                    },
                    series: [
                        {
                            yAxis: 0,
                            data: data_tempIn.slice(5),
                        },
                        {
                            yAxis: 0,
                            data: data_tempOut.slice(5),
                        }
                    ],
                },
            }]
        }
    });

    const dbRef_dataRecord = firebase.database().ref().child('codeSerre/' + codeSerre + '/dataRecord');
    // Focntion pour detecter les nouvelles valeurs
    dbRef_dataRecord.on('value', snap => {
        chart_dataEx.series[0].setData(data_tempIn, true); // Changement pour la température
        chart_dataEx.series[1].setData(data_tempOut, true); // Changement pour l'humidité
    });
}
