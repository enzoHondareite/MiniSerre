var array_JSON_dataRecord = []; // Variable (tableau) pour prendre le JSON de firebase et le mettre dans un array

var data_tempOut = []; // Variable (tableau) pour prendre les valeurs de la température intérieur pour highChart
var data_humOut = []; // Variable (tableau) pour prendre les valeurs de l'humidité intérieur pour highChart

var data_tempIn = []; // Variable (tableau) pour prendre les valeurs de la température extérieur pour highChart
var data_humIn = []; // Variable (tableau) pour prendre les valeurs de l'humidité extérieur pour highChart
var data_humSoil = []; // Variable (tableau) pour prendre les valeurs de l'humidité du sol pour highChart

function readDatabase(codeSerre) {
    // Chemin d'accés pour "dataRecord" dans la bdd
    const dbRef_dataRecord = firebase.database().ref().child('codeSerre/' + codeSerre + '/dataRecord');
    // Chemin d'accés pour "setting" dans la bdd
    const dbRef_setting = firebase.database().ref().child('codeSerre/' + codeSerre + '/setting');

    // Chemin d'accés pour "show_waterLevel" dans la page HTML
    const show_waterLevel = document.getElementById('show_waterLevel');
    // Chemin d'accés pour "show_electricityLevel" dans la page HTML
    const show_electricityLevel = document.getElementById('show_electricityLevel');


    // Requête des 11 premières valeurs de "dataRecord" dans la bdd
    dbRef_dataRecord.limitToFirst(11).on('value', snap => {
        // Variable pour récupérer le fichier JSON renvoyé par Firebase
        var JSON_dataRecord = snap.val();

        /* Remise à zèro de toute les variables de données pour mettre les nouvelles */
        array_JSON_dataRecord = [];

        data_tempOut = [];
        data_humOut = [];

        data_tempIn = [];
        data_humIn = [];
        data_humSoil = [];
        /*																			  */

        // Fonction for pour boucler dans le JSON et extraire ces valeurs
        for (var x in JSON_dataRecord) {
            array_JSON_dataRecord.push(JSON_dataRecord[x]); // Ajoute les valeur dans un array un par un grace à x
        }
        

        // Fonction for pour boucler dans l'array pour trier et associer les données
        for (var x in array_JSON_dataRecord) {
            // Association du timestamp et de la température extérieur
            
            data_tempOut.push({
                x: (new Date(array_JSON_dataRecord[x]['timeStamp']).getTime() + new Date().getTimezoneOffset() * (-1)) * 1000, // timestamp
                y: array_JSON_dataRecord[x]['outData']['temperature'], // température
            });

            // Association du timestamp et de l'humidité extérieur
            data_humOut.push({
                x: (new Date(array_JSON_dataRecord[x]['timeStamp']).getTime() + new Date().getTimezoneOffset() * (-1)) * 1000, // timestamp + décalage horaire
                y: array_JSON_dataRecord[x]['outData']['humidity'], // humidité
            });

            // Association du timestamp et de température intérieur
            data_tempIn.push({
                x: (new Date(array_JSON_dataRecord[x]['timeStamp']).getTime() + new Date().getTimezoneOffset() * (-1)) * 1000, // timestamp + décalage horaire
                y: array_JSON_dataRecord[x]['inData']['temperature'], // température
            });

            // Association du timestamp et de l'humidité intérieur
            data_humIn.push({
                x: (new Date(array_JSON_dataRecord[x]['timeStamp']).getTime() + new Date().getTimezoneOffset() * (-1)) * 1000, // timestamp + décalage horaire
                y: array_JSON_dataRecord[x]['inData']['humidity'], // humidité
            });

            // Association du timestamp et de l'humidité du sol
            data_humSoil.push({
                x: (new Date(array_JSON_dataRecord[x]['timeStamp']).getTime() + new Date().getTimezoneOffset() * (-1)) * 1000, // timestamp + décalage horaire
                y: array_JSON_dataRecord[x]['inData']['humiditySoil'], // humidité sol
            });
        }
    });

    // Requête des deux paramétres valeurs de "setting" dans la bdd
    dbRef_setting.on('value', snap => {
        // Variable pour récupérer le fichier JSON renvoyé par Firebase
        var JSON_setting = snap.val();

        /* Remise à zèro de toute les variables de données pour mettre les nouvelles */
        var array_JSON_setting = [];

        // Insertion des données dans la page HTML + le suffixe
        if(JSON_setting['waterLevel'] == 2) {
            show_waterLevel.innerText = "> 75%";
        } else if(JSON_setting['waterLevel'] == 1) {
            show_waterLevel.innerText = "< 75%";
        } else if(JSON_setting['waterLevel'] == 0) {
            show_waterLevel.innerText = "< 25%";
        }


        show_electricityLevel.innerText = JSON_setting['electricityLevel'] + " %";

        // Appel à la fonction pour choisir le badge Bootstrap
        selectworkingOrder(JSON_setting['waterLevel'], JSON_setting['electricityLevel']);
        // Appel à la fonction pour calculer l'autonomie
        autonomyBat(JSON_setting['electricityLevel']);
    });
}
