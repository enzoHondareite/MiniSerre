function displayGraphIn(codeSerre) {
    // Graphique pour les données extérieur : température, humidité, himidité sol
    var chart_dataIn = Highcharts.chart('showDataIn', {
        chart: {
            type: 'spline', // Ligne lisse
            backgroundColor: 'rgba(0, 0, 0, 0.35)',
            animation: {
                duration: 500,
            },
        },

        title: {
            text: 'Humidité de la serre',
            style: {
                color: '#FFF',
            },
        },

        xAxis: {
            type: 'datetime',
            title: {
                text: 'Heure',
                style: {
                    color: '#FFF',
                },
            },
            labels: {
                style: {
                    color: '#FFF',
                },
            },
            tickInterval: 600000, // Intervalle des labels en timestamp (10 min)
        },

        yAxis: [{
            title: {
                text: 'Humidité (%)',
                style: {
                    color: '#FFF',
                    fontSize: '13px',
                },
            },
            labels: {
                style: {
                    color: '#7CB5EC',
                    fontSize: '13px',
                    fontWeight: 'bold',
                },
                format: '{value} %', // Terminaison du label
            },
            max: 100,
            min: 0
        }],

        tooltip: {
            shared: true,
            headerFormat: 'Heure : <b>{point.x:%H:%M}</b><br/>', // Titre tooltip = "Heure : H:M:S"
            pointFormat: '{series.name}: <b>{point.y}</b><br>' // Tooltip = Température : y / Humidité : y / Himdité du sol : y
        },

        legend: {
            itemStyle: {
                color: '#FFF',
            },
        },

        plotOptions: { // Ligne
            series: {
                lineWidth: 4,
                marker: {
                    radius: 7, // Point
                }
            }
        },

        series: [
            {
                yAxis: 0,
                name: 'Humidité serre',
                data: data_humIn,
                tooltip: {
                    valueSuffix: ' %',
                },
                color: '#FF0000'
            },
            {
                yAxis: 0,
                name: 'Humidité du sol',
                data: data_humSoil,
                tooltip: {
                    valueSuffix: ' %',
                },
                color: '#00FF00'

            },
            {
                yAxis: 0,
                name: 'Humidité extérieur',
                data: data_humOut,
                tooltip: {
                    valueSuffix: ' %'
                }
            }
        ],


        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500,
                },
                chartOptions: {
                    legend: {
                        enabled: false,
                    },
                    plotOptions: {
                        series: {
                            lineWidth: 3,
                            marker: {
                                radius: 4, // Point
                            },
                        },
                    },
                    series: [
                        {
                            yAxis: 0,
                            data: data_humIn.slice(5),
                        },
                        {
                            yAxis: 0,
                            data: data_humSoil.slice(5),
                        },
                        {
                            yAxis: 0,
                            data: data_humOut.slice(5),
                        }
                    ],
                },
            }]
        }
    });

    const dbRef_dataRecord = firebase.database().ref().child('codeSerre/' + codeSerre + '/dataRecord');
    // Focntion pour detecter les nouvelles valeurs
    dbRef_dataRecord.on('value', snap => {
        chart_dataIn.series[0].setData(data_humIn, true); // Changement pour la température
        chart_dataIn.series[1].setData(data_humSoil, true); // Changement pour l'humidité
        chart_dataIn.series[2].setData(data_humOut, true); // Changement pour l'humidité du sol
    });
}
